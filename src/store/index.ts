import { create } from "zustand";
import { devtools, persist } from "zustand/middleware";
import { TaskInterface } from "../interfaces/Task";

interface TaskState {
  tasks: TaskInterface[];
  add: (by: TaskInterface) => void;
  sort: (by: TaskInterface[]) => void;
  toggle: (id: string) => void;
}

export const useTaskStore = create<TaskState>()(
  devtools(
    persist(
      (set) => ({
        tasks: [],
        add: (task) => set((state) => ({ tasks: [...state.tasks, task] })),
        sort: (tasks) => set((state) => ({ tasks: [...tasks] })),
        toggle: (index) =>
          set((state) => {
            const task = state.tasks.find((task) => task.id === index);

            if (!task) return { tasks: [...state.tasks] };

            task.completed = !task.completed;
            return { tasks: [...state.tasks] };
          }),
      }),
      {
        name: "task-storage",
      }
    )
  )
);

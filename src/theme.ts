import * as React from 'react';
import { purple, red } from '@mui/material/colors';
import { ThemeProvider, createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
    secondary: {
        main: "#F6F6F6"
    }
  },
});
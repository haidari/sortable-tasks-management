import { UUID } from "crypto";

export interface TaskInterface {
    title: string;
    description: string;
    completed: boolean
    order: number
    id: string
}
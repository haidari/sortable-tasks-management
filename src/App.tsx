import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Button from "@mui/material/Button/Button";
import { Container } from "@mui/system";
import { InputTask } from "./components/inputTask";
import Card from "@mui/material/Card/Card";
import Box from "@mui/material/Box";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import { theme } from "./theme";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import CardMedia from "@mui/material/CardMedia";
import CardActionArea from "@mui/material/CardActionArea";
import Stack from "@mui/material/Stack";
import { TasksList } from "./components/tasksList";

function App() {
  return (
    <div className="App">
      <Container>
        <h1> Task Management</h1>

        <InputTask />

        <TasksList />


      </Container>
    </div>
  );
}

export default App;

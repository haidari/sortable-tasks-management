import Stack from "@mui/material/Stack/Stack";
import { useTaskStore } from "../store";
import { Task } from "./task";
import { ReactSortable } from "react-sortablejs";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useState } from "react";
import Box from "@mui/material/Box";

enum Status  {
    All,
    Not_Completed,
    Completed
}

export function TasksList() {
  const tasks = useTaskStore((state) => state.tasks);
  const sort = useTaskStore((state) => state.sort);
  const [filter, setFilter] = useState<Status>(Status.All);



  function handleFilter(event: SelectChangeEvent<unknown>) {
    console.log(event.target.value);
    setFilter(event.target.value as Status);
  }
  return (
    <>
      <Box sx={{ py: 2 }}>
        <Select
          id="filter"
          label="Filter"
          value={filter}
          onChange={handleFilter}
          size={"medium"}
        >
          <MenuItem value={Status.All}>All</MenuItem>
          <MenuItem value={Status.Completed}>Completed</MenuItem>
          <MenuItem value={Status.Not_Completed}>Not Completed</MenuItem>
        </Select>
      </Box>

      <ReactSortable list={tasks} setList={sort}>
        {tasks
          .filter((task) =>
            filter == Status.Completed
              ? task.completed
              : filter == Status.Not_Completed
              ? !task.completed
              : true
          )
          .map((task) => (
            <Task
              key={task.id}
              title={task.title}
              description={task.description}
              id={task.id}
              completed={task.completed}
              order={task.order}
            />
          ))}
      </ReactSortable>
    </>
  );
}

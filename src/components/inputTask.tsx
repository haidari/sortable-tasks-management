import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { useState } from "react";
import { useTaskStore } from "../store";
import uuid from "react-uuid";
import Button from "@mui/material/Button/Button";
import { Formik, Form, Field, useFormik } from "formik";
import * as Yup from "yup";
import Box from "@mui/material/Box";

export function InputTask() {
  const TaskSchema = Yup.object().shape({
    title: Yup.string().min(2, "Too Short!").required("Required"),
    description: Yup.string().min(2, "Too Short!").optional(),
  });

  const addTaskToStore = useTaskStore((state) => state.add);

  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
    },
    validationSchema: TaskSchema,
    onSubmit: ({ title, description }, {resetForm}) => {
      addTaskToStore({
        title: title,
        description: description,
        id: uuid(),
        completed: false,
        order: 0,
      });

      resetForm();

    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Task Title"
            id="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.title && Boolean(formik.errors.title)}
            helperText={formik.touched.title && formik.errors.title}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Task Description"
            id="description"
            value={formik.values.description}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={
              formik.touched.description && Boolean(formik.errors.description)
            }
            helperText={formik.touched.description && formik.errors.description}
          />

          <Grid xs={12}>
            <Box sx={{ py: 2 }}>
              <Button size="large" variant="contained" type="submit">
                Add Task
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
}

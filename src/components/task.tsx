import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography";
import { TaskInterface } from "../interfaces/Task";
import { useTaskStore } from "../store";

export function Task(props: TaskInterface) {
  const toggleTaskInStore = useTaskStore((state) => state.toggle);

  function toggleTask() {
    toggleTaskInStore(props.id);
  }
  return (
    <Box sx={{ p: 2 }}>
      <Card>
        <CardActionArea>
          <CardContent>
            <Typography
              textAlign={"left"}
              gutterBottom
              variant="h5"
              component="div"
            >
              <Checkbox onChange={toggleTask} checked={props.completed} />{" "}
              <span
                style={{
                  textDecoration: props.completed ? "line-through" : "none",
                }}
              >
                {" "}
                {props.title}{" "}
              </span>
            </Typography>
            <Box sx={{p: 2}}>
            <Typography variant="body2" color="text.secondary">
              {props.description}
            </Typography>
            </Box>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
}
